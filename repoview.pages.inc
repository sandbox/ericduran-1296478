<?php

/**
 * @file
 *   
 */


/**
 * Title callback for repoview.
 */
function repoview_view_repository_title_callback($versioncontol_repository) {
  return t('Repository contents for @reposiotry_name', array('@reposiotry_name' => $versioncontol_repository->name));
}

/**
 * Page callback for repoview.
 */
function repoview_view_repository_callback(VersioncontrolRepository $versioncontol_repository) {
  $output = '';
  // Get the repository data for this particular repository.
  $repo_data = repoview_get_repository_page_details($versioncontol_repository);
  if (!$content = repoview_view_object_from_path($repo_data)) {
    drupal_not_found();
  }
  else {
    $navigation = repoview_get_browser_navigation($repo_data);
    repoview_set_breadcrumb($repo_data);
    $output = theme('repoview_browser', array('navigation' => $navigation, 'content' => $content));
  }
  return $output; 
}

/**
 *
 */
function repoview_view_object_from_path($repo_data) {

  $hash = md5(serialize($repo_data));
  //if ($cache = cache_get('repoview_object__' . $hash)) { 
  if (FALSE) { 
    $data = $cache->data;
    // TODO: Determine if there are any circumstances under which we might need to do cache invalidation
  }
  else {
    // Get the object we are trying to view.
    if (!$data = repoview_get_object_from_path($repo_data)) {
      return FALSE;
    }
    cache_set('repoview_object__' . $hash, $data);
  }
  $name = $data->name ? $data->name : $repo_data['repository']->name;
  // Determine whether we are trying to view a folder or a file.
  if ($data->type == 'directory') {
    return $content = theme('repoview_view_directory', array('contents' => $data->contents, 'name' => $name));
  }
  elseif ($data->type == 'file') {
    return $content = theme('repoview_view_file',  array('contents' => $data->contents, 'name' => $name));
  }
  // If we have neither a directory or a file, return page not found.
  else {
    return FALSE;
  }
  return $output;
}

/**
 * View the contents of an individual commit.
 */
function repoview_view_commit_callback(VersioncontrolRepository $versioncontrol_repository) {
  // Sanity check to make sure that this commit id is only numbers and
  // letters and does not attempt to execute aribtrary code on our
  // 'git-show system call'.
  // TODO: This sanity check only checks git commits, defer this to the handler?
  if (preg_match('/^[a-zA-Z0-9]+$/', $_GET['commit']) && ctype_xdigit($_GET['commit'])) {
    $data = repoview_get_repository_page_details($versioncontrol_repository);
    if ($commit_diff = repoview_handler_factory($versioncontrol_repository)->getCommit($data['repository']->root, $data['branch'], $data['commit'])) {
      $output = repoview_format_text($commit_diff->diff, 'diff');
      repoview_set_breadcrumb($data);
      return $output;
    }
    else {
      drupal_set_message(t('The commit you were looking for was not found'), 'error');
    }
  }
  else {
    drupal_set_message(t('Invalid commit id.'), 'error');
  }
  drupal_not_found();
}

/**
 * Build the links necessary for the browser navigation.
 */
function repoview_get_browser_navigation(array $repo_data) {

  drupal_add_css(drupal_get_path('module', 'repoview') . '/css/navigation.css');
  drupal_add_js(drupal_get_path('module', 'repoview') . '/js/git_repo_viewer.js');

  $branches_form = drupal_get_form('repoview_branch_form', $repo_data);
  $repo_data['path'] != '' ? $input_format_token_form = drupal_render(drupal_get_form('repoview_input_token_form', $repo_data)) : $input_format_token_form = '';

  // Look up previous commit.
  if (isset($repo_data['previous_commits'][0]) && $repo_data['previous_commits'][0] != '') {
    $link_data = array('commit' => $repo_data['previous_commits'][0]);
    if (isset($repo_data['path'])) {
      $link_data['path'] = $repo_data['path'];
    }
    $previous_commit_link = repoview_get_link(t('previous commit'), $link_data);
  }
  else {
    $previous_commit_link = '';
  }
  // TODO: We should be able to get the next commit from our MySQL data.
  $next_commit_link = repoview_get_link(t('next commit'), array('commit' => sha1_hex($repo_data['previous_commits'])));
  $next_commit_link = '';
  
  // Get the parent folder.
  $parent_folder_path = explode('/', $repo_data['path']);
  array_pop($parent_folder_path);
  $parent_folder_path = implode('/', $parent_folder_path);
  $parent_folder_link = repoview_get_link(t('parent folder'), array('path' => $parent_folder_path));
  
  // Build the history link.
  $history = module_exists('commitlog') ? l(t('history'), 'commitlog/repository/' . $repo_data['repository']->repo_id) : NULL;
  return theme('repoview_navigation', array(
    'previous_commit_link' => $previous_commit_link,
    'next_commit_link' => $next_commit_link,
    'parent_folder_link' =>  $parent_folder_link,
    'history' => $history,
    'branches_form' => drupal_render($branches_form),
    'input_format_token_form' => $input_format_token_form
  ));
}

/**
 * Remap known Drupal file extensions to the one appropriate for Geshi.
 *
 * Currently this cannot be overridden or extended but a hook should be added
 * here if it ever becomes an issue.
 */
function repoview_get_file_type($name) {
  $ext = end(explode('.', $name));
  $type = $ext;
  $drupal_extensions = array(
    'module',
    'install',
    'test',
    'inc',
    'engine',
  );
  if (in_array($ext, $drupal_extensions)) {
    $type = 'php';
  }
  if ($name == 'js') {
    $type = 'javascript';
  }
  return $type;
}

/**
 * Set the breadcrumbs for the repo browser.
 */
function repoview_set_breadcrumb($repodata) {
  $path = explode('/', $repodata['path']);
  unset($repodata['path']);
  unset($repodata['previous_commits']);
  $breadcrumbs = array(
    l(t('Home'), '<front>'),
    repoview_get_link($repodata['repository']->name),
  );
  $current_path = '';
  foreach ($path as $item) {
    if ($path != '') {
      $item = $current_path . '/' . $item;
      $info['path'] = $item;
      $breadcrumbs[] = repoview_get_link($item, $info);
    }
  }
  drupal_set_breadcrumb($breadcrumbs);
}

/**
 * Provides the 
 */
function repoview_commit_list_block() {
  // TODO: consider whether we can make this with views?  (currently we can't without a lot of work)
  // TODO: figure out if this is really working the way it is meant to.
  // TODO: Comment this, it's definitely not obvious what it is trying to do.
  $info = repoview_get_repository_page_details();
  $current_op_id = dbtng_query('SELECT vc_op_id FROM {versioncontrol_operations} WHERE revision=:commit', array(':commit' => $info['commit']))->fetchField();
  $query = db_select('versioncontrol_operations', 'vo')
    ->fields('vo', array('message', 'revision'));
  $query->join('versioncontrol_operation_labels', 'vol', 'vo.vc_op_id = vol.vc_op_id');
  $query->join('versioncontrol_labels', 'vl', 'vol.label_id = vl.label_id');
  $query->condition('vl.name', $info['branch'])
    ->condition('vo.repo_id', $info['repository']->repo_id);
  $before = $query;
  $before_results = $before->orderBy('vo.vc_op_id', 'DESC')
    ->condition('vo.vc_op_id', $current_op_id, '>=')
    ->range(0, 10)
    ->execute()
    ->fetchAll();
  $after = $query;
  $after_results = $before->orderBy('vo.vc_op_id', 'DESC')
    ->condition('vo.vc_op_id', $current_op_id, '<')
    ->range(0, 10 - count($before_results))
    ->execute()
    ->fetchAll();
  $results = array_merge($before_results, $after_results);
  $items = array();
  // TODO: this is get specific, move it!
  $git_backend = new VersioncontrolGitBackend;
  foreach ($results as $commit) {
    $commit_link = repoview_get_link($git_backend->formatRevisionIdentifier($commit->revision, 'short') . ': ' . check_plain($commit->message), array('commit' => $commit->revision));
    $items[] = $commit_link;
  }
  return $items;
}


/**
 * Provide a form to switch between repository branches.
 *
 * @param $form_state
 *   The Drupal FAPI form state array.
 * @param $repo_data
 *   The repoview repo data array.
 */
function repoview_branch_form($form, $form_state, $repo_data) {
  $options = array();
  foreach ($repo_data['repository']->loadBranches() as $branch) {
    $options[$branch->name] = $branch->name;
  }
  $form['branches'] = array(
    '#title' => t('Branches'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $repo_data['branch'],
  );
  return $form;
}

/**
 * Default theme implementation for repoview_view_file.
 *
 * @param (fileContents) $file_contents
 *   The actual contents of the file being viewed.
 * @param (string) $name
 *   The name of the file.
 */
function theme_repoview_view_file($variables) {
  $file_contents = $variables['contents'];
  $name = $variables['name'];
  $start_line = (!empty($variables['start_line']) ? $variables['start_line'] : 1);
  $type = repoview_get_file_type($name);
  $image_extensions = array(
    'jpg',
    'jpeg',
    'gif',
    'png',
    'ico',
  );
  if (in_array($type, $image_extensions)) {
    $output = theme('repoview_view_image', base64_encode($file_contents), $type);
  }
  else {
    // TODO: turn this into a theme function?
    $output = repoview_format_text($file_contents, $type, $start_line);
  }
  return $output;
}

