<?php
/**
 * @file
 */

/**
 * Default theme implementation for repoview_view_directory.
 */
function theme_repoview_view_directory($variables) {
  $contents = $variables['contents'];
  $name = $variables['name'];
  // Create the header columns for our table.
  drupal_add_css(drupal_get_path('module', 'repoview') . '/css/folder.css');
    $header = array(
      t('Name'),
      t('Date'),
      t('Author'),
      t('Commit'),
      t('Message'),
    );
    // WE NEED TO FIND OUR PATHS FROM THE ROOT REPOSITORY HERE...
    // TODO: figure out what this actually meant and if it's already done?
    $data = repoview_get_repository_page_details();
    $base_path = $data['path'] == '' ? '' : '/' . $data['path'];
    $paths = array();
    foreach ($contents as $name => $value) {
      $paths[] = array(
        'path' => $base_path . '/' . $name,
        'dir' => $value['directory'],
      );
    }
    // to make sure this is the case (on d.o right now this WILL NOT WORK).
    $op_id = db_query('SELECT vc_op_id FROM {versioncontrol_operations} WHERE revision=:commit', array(':commit' => $data['commit']))->fetchField();
    $query = db_select('versioncontrol_item_revisions', 'vir')
      ->condition('vir.path', $paths, 'IN')
      ->condition('vir.repo_id', $data['repository']->repo_id);
    $or = db_or();
    // TODO: Right now relying on data coming into the db in a specific order.  We need
    foreach ($paths as $path) {
      if (!empty($path['dir'])) {
        // TODO: Finish this up.  The goal here is to find matches for directories based on the most recently modified child items.
        $or->condition($path['path'] . '%', 'LIKE');
      }
      else {
        $query->condition('vir.path', substr($path['path'], 0, -1) . '%', 'LIKE');
      }
    }
    //TODO: Eric
    // $query->condition($or);
    $query->join('versioncontrol_operations', 'vo', 'vir.vc_op_id = vo.vc_op_id');
    $query->join('versioncontrol_operation_labels', 'vol', 'vo.vc_op_id = vol.vc_op_id');
    $query->join('versioncontrol_labels', 'vl', 'vol.label_id = vl.label_id');
    $results = $query
      ->fields('vir', array('path', 'revision'))
      ->fields('vo', array('author_date', 'committer', 'committer_uid', 'message'))
      ->condition('vl.name', $data['branch'])
      ->condition('vo.vc_op_id', $op_id, '<=')
      ->distinct('path')
      ->addTag('repoview_directory_list_metadata')
      ->execute()
      ->fetchAllAssoc('path');
    $git_backend = new VersioncontrolGitBackend;
    $rows = array();
    // TODO: If we don't have the last modified commit info for a file it's because the file was last modified in another branch.
    // We could add something here to retrieve that data from the backend (for git -- git log -p [path/within/repo]
    // TODO: create a service that can be used to get a list of last changed commits and messages out of the repository
    foreach ($contents as $name => $item) {
      $item_data = new stdClass;
      $item_path = $base_path . '/' . $name;
      if (isset($results[$item_path])) {
        $item_data = $results[$item_path];
      }
      if (isset($item_data->revision)) {
        $commit_link = repoview_get_link($git_backend->formatRevisionIdentifier($item_data->revision, 'short'), array('commit' => $item_data->revision));
      }
      if (isset($item_data->message) && $item_data->message) {
        $message_link = l($item_data->message, 'repoview/commit/' . $data['repository']->repo_id, array('query' => array('commit' => $item_data->revision)));
      }
      else {
        $message_link = '';
      }
      if (isset($item_data->committer_uid) && $item_data->committer_uid != 0) {
        $author = l(user_load($item_data->committer_uid)->name, 'user/' . $item_data->committer_uid);
      }
      elseif (isset($item_data->committer)) {
        $author = $item_data->committer;
      }
      else {
        $author = '';
      }
      $rows[] = array(
        'data' => array(
          'name' => array(
            'data' => repoview_get_link_to_node($item),
            'class' => $item['directory'] ? 'folder' : 'file',
          ),
          'date' => isset($item_data->date) ? t('@time ago', array('@time' => format_interval(time() - $item_data->date))) : '',
          'author' => $author,
          'commit' => isset($commit_link) ? $commit_link : '',
          'message' => $message_link,
        ),
      );
    }
  return theme('table', array('header' => $header, 'rows' => $rows));
}
