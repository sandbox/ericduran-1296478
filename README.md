# Git Repository Viewer

The Git Repository Viewer is a repository browser for Drupal that integrates with the versioncontrol suite of modules to provide an intuitive repository browser for Drupal.

It is currently under active development and is currently unstable.  Feel free to download it and try it out.

## Installation

1.  Git must be installed (and accessible to the apache user for a call to `git show 'commit'`)
2.  Install versioncontrol_git, versioncontrol and all of its dependencies
3.  Download [glip](https://github.com/halstead/glip.git) file from https://github.com/halstead/glip/zipball/1.0 or run: `git clone --branch=1.0 git://github.com/halstead/glip.git`
4.  Place the glip directory at sites/all/libraries/glip so that you have `sites/all/libraries/glip/lib`


## For best results

1.  Download and install the [Geshi Filter Module](http://drupal.org/project/geshifilter)
2.  Download and install the [Markdown Filter Module](http://drupal.org/project/markdown)
