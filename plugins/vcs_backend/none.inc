<?php

$plugin = array(
  'title' => t('None'),
  'description' => t('A placeholder backend handler that always returns reporting that the object was not found.'),
  'handler' => array(
    'class' => 'RepoviewNone',
  ),
  'backends' => array('all'),
);
