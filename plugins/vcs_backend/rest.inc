<?php

$plugin = array(
  'title' => t('REST'),
  'description' => t('Makes get requests to receive JSON data at a configurable URL in order to retrieve contents of the repository.'),
  'handler' => array(
    'class' => 'RepoviewREST',
  ),
  'backends' => array('all'),
);
