<?php

$plugin = array(
  'title' => t('Use Glip to check the local disk'),
  'description' => t('Uses the Glip libraries to read the files from a locally accessible disk.'),
  'handler' => array(
    'class' => 'RepoviewGlip',
  ),
  'backends' => array('git'),
);
