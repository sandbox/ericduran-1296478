<?php
class RepoviewREST implements RepoviewRepoInterface {

  public $server = '';
  
  /** 
   *
   */
  public function __construct() {
  }

  /**
   * Get all relevant metadata about a repository object.
   */
  public function getRepoData($path, $branch, $commit = FALSE) {

  }

  /** 
   * Get a representation of an Object within the Git Repository.
   *
   * @param string $repo_location
   *   The path to the actual repository on disk.
   * @param string $path
   *   The path withihin the repository for which we are trying to retrieve an object.
   * @param string $branch
   *   The branch on which we are trying to view this object.
   *   TODO: This is here out of a vague suspicion that it'll be critical for svn
   *   (if this is abstracted that far).  Should it be?
   * @param string $commit
   *   The revision at which we are trying to view this object.
   *
   * @return mixed
   *   On failure, FALSE.
   *   On success, a stdClass object whose attributes are as follows:
   *     @code
   *        // The name of this element.
   *        $data->name '';
   *        // The type of this element, either file or directory.
   *        $data->type '';
   *        // The contents of this element, for files the content of the file for
   *        // directories a multidimmenstional array.
   *        $data->contents '';
   *     @endcode
   */
  public function getObject($repo_location, $path, $branch, $commit) {
    $data['repo_location'] = $repo_location;
    $data['branch'] = $branch;
    $data['commit'] = $commit;
    $this->sendRequest('getCommit', $data);
  }


  /** 
   * Get a representation of an Object within the Git Repository.
   *
   * @param string $repo_location
   *   The path to the actual repository on disk.
   * @param string $branch
   *   The branch on which we are trying to view this object.
   *   TODO: This is here out of a vague suspicion that it'll be critical for svn
   *   (if this is abstracted that far).  Should it be?
   * @param string $commit
   *   The revision at which we are trying to view this object.
   *
   * @return string
   *   The contents of the commit.
   */
  public function getCommit($repo_location, $branch, $commit) {
    $data['repo_location'] = $repo_location;
    $data['branch'] = $branch;
    $data['commit'] = $commit;
    $data = $this->sendRequest('getCommit', $data);
  }

  /**
   * Send a 
   */
  private function sendRequest($method, $data) {
    $post_data['method'] = $method;
    $post_data['data'] = $data;
    $response_data = drupal_http_request($this->server, array('Content-Type' => 'application/x-www-form-urlencoded'), 'POST', http_build_query($post_data));
    if (!$data = json_decode($response_data->data)) {
      throw new Exception('Invalid data received from remote.');
    }
    return $response_data->result;
  }

  public function findPathLastModified($repo_location, $branch, $commit, $path) {
  }

  public function form() {
    $form = array();
    $form['server'] = array(
      '#title' => t('Server URL'),
      '#description' => t('The URL of the server from which to retrieve the repository contents.'),
      '#type' => 'textfield',
      '#default_value' => '',
    );
    return $form;
  }

  public function configure($settings) {
  }
}
