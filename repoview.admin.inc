<?php
/**
 * @file
 *   Provide the administration interface for the repoview.
 */

/**
 * The form definition for the administrative settings form.
 */
function repoview_settings_form($form, &$form_state) {
  $form['help'] = array(
    '#type' => 'markup',
    '#value' => '<p>' . t('Settings on this page provide defaults for each versioncontrol backend that may be overridden on the individual repository.') . '</p>',
  );
  // Loop over the available backends building configuration for each one.
  foreach (versioncontrol_get_backends() as $backend) {
    $settings = variable_get('repoview_' . $backend->type, array('handler' => 'none'));
    // PROBLAMATIC FOR NOW Eric J. Duran
    $form += repoviewer_get_backend_settings_form($backend, $settings);
  }
  return system_settings_form($form);
}

/**
 * 
 */
function repoviewer_get_backend_settings_form($backend, $settings) {
  ctools_include('dependent');
  ctools_include('plugins');
  $form = array();
  $form_options = array();
  // Get applicable plugins for each available backend type.
  $plugins = repoview_get_plugins_for_backend($backend->type);
  foreach ($plugins as $plugin) {
    $form_options[$plugin['name']] = '<strong>' . $plugin['title'] . '</strong><p>' . $plugin['description'] . '</p>';
  }
  $form['repoview_' . $backend->type] = array(
    '#type' => 'fieldset',
    '#title' => t('@backend Settings', array('@backend' => $backend->name)),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['repoview_' . $backend->type]['handler'] = array(
    '#title' => t('Default @backend handler plugin', array('@backend' => $backend->name)),
    '#type' => 'radios',
    '#description' => t('Select the handler to use by default for all @backend repositories', array('@backend' => $backend->name)),
    '#options' => $form_options,
    '#default_value' => $settings['handler'],
    '#name' => 'repoview_git_handler',
  );
  foreach ($plugins as $plugin) {
    // TODO: Review this too, I did some quick fixes to stop it from errors out,
    // But thats about it. Eric J. Duran.
    $handler = repoview_get_repo_backend_handler($plugin['name']);
    if (method_exists($handler, 'form')) {
      $form['repoview_' . $backend->type]['repoview_' . $plugin['name'] . '_settings'] = array(
        '#title' => t('@plugin plugin settings', array('@plugin' => $plugin['title'])),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
        '#input' => TRUE,
        '#process' => array('ctools_dependent_process'),
        '#dependency' => array(
          'radio:repoview_' . $backend->type . '[handler]' => array(
            $plugin['name'],
           ),
         ),
         '#id' => 'repoview-' . $handler->plugin_info['name'] . '-settings',
         '#prefix' => '<div id="repoview-' . $handler->plugin_info['name'] . '-settings-wrapper">',
         '#suffix' => '</div>',
        '#description' => t('These settings will apply to all repositories of this type unless these settings are overwritten on the individual repository.'),
      );
      $form['repoview_' . $backend->type]['repoview_' . $plugin['name'] . '_settings']['#tree'] = TRUE;
      $form['repoview_' . $backend->type]['repoview_' . $plugin['name'] . '_settings']['settings'] = $handler->form();
    }
  }
  return $form;
}
